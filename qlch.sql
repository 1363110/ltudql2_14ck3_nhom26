USE [QLCH]
GO
/****** Object:  Table [dbo].[NhaCungCap]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NhaCungCap]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NhaCungCap](
	[MaNhaCungCap] [int] IDENTITY(1,1) NOT NULL,
	[TenNhaCungCap] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[DienThoai] [nvarchar](15) NULL,
 CONSTRAINT [PK_NhaCungCap] PRIMARY KEY CLUSTERED 
(
	[MaNhaCungCap] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[NhaCungCap] ON
INSERT [dbo].[NhaCungCap] ([MaNhaCungCap], [TenNhaCungCap], [DiaChi], [DienThoai]) VALUES (1, N'Lego Việt Nam', N'Hà Nội', N'0126 211 8488')
INSERT [dbo].[NhaCungCap] ([MaNhaCungCap], [TenNhaCungCap], [DiaChi], [DienThoai]) VALUES (2, N'Kinh Đô', N' tp.Hồ Chí Minh', N' 08 3751 0298')
INSERT [dbo].[NhaCungCap] ([MaNhaCungCap], [TenNhaCungCap], [DiaChi], [DienThoai]) VALUES (3, N'Unilever Vietnam Ltd', N'Bình Dương', N'064 3769 151')
INSERT [dbo].[NhaCungCap] ([MaNhaCungCap], [TenNhaCungCap], [DiaChi], [DienThoai]) VALUES (4, N'VPP Vĩnh Phát', N'Tp HCM', N'08 3969 8727')
INSERT [dbo].[NhaCungCap] ([MaNhaCungCap], [TenNhaCungCap], [DiaChi], [DienThoai]) VALUES (5, N'Hòa Phát', N' tp.Hồ Chí Minh', N'08 3840 4613')
INSERT [dbo].[NhaCungCap] ([MaNhaCungCap], [TenNhaCungCap], [DiaChi], [DienThoai]) VALUES (6, N'Vinamilk', N'VN', N'0123')
INSERT [dbo].[NhaCungCap] ([MaNhaCungCap], [TenNhaCungCap], [DiaChi], [DienThoai]) VALUES (7, N'P&G Việt Nam', N'tp.Hồ Chí Minh', N'08 3822 5678')
INSERT [dbo].[NhaCungCap] ([MaNhaCungCap], [TenNhaCungCap], [DiaChi], [DienThoai]) VALUES (8, N'Trung Nguyên', N'', N'')
SET IDENTITY_INSERT [dbo].[NhaCungCap] OFF
/****** Object:  Table [dbo].[DanhMuc]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DanhMuc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DanhMuc](
	[MaDanhMuc] [int] IDENTITY(1,1) NOT NULL,
	[TenDanhMuc] [nvarchar](50) NULL,
	[MoTa] [ntext] NULL,
 CONSTRAINT [PK_DanhMuc] PRIMARY KEY NONCLUSTERED 
(
	[MaDanhMuc] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[DanhMuc] ON
INSERT [dbo].[DanhMuc] ([MaDanhMuc], [TenDanhMuc], [MoTa]) VALUES (1, N'Đồ chơi', N'Đồ chơi trẻ em')
INSERT [dbo].[DanhMuc] ([MaDanhMuc], [TenDanhMuc], [MoTa]) VALUES (2, N'Thời trang', N'quần áo, giày dép, mũ nón...')
INSERT [dbo].[DanhMuc] ([MaDanhMuc], [TenDanhMuc], [MoTa]) VALUES (3, N'Thực phẩm', N'Rau, củ, quả, hạt')
INSERT [dbo].[DanhMuc] ([MaDanhMuc], [TenDanhMuc], [MoTa]) VALUES (6, N'Đồ gia dụng', N'Vật dụng hàng ngày,,')
INSERT [dbo].[DanhMuc] ([MaDanhMuc], [TenDanhMuc], [MoTa]) VALUES (4, N'Mỹ phẩm', N'Hóa phẩm, dược liệu')
INSERT [dbo].[DanhMuc] ([MaDanhMuc], [TenDanhMuc], [MoTa]) VALUES (5, N'Văn phòng phẩm', N'Tập, bút,...')
INSERT [dbo].[DanhMuc] ([MaDanhMuc], [TenDanhMuc], [MoTa]) VALUES (7, N'danh mục', N'')
SET IDENTITY_INSERT [dbo].[DanhMuc] OFF
/****** Object:  Table [dbo].[LoaiNguoiDung]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoaiNguoiDung]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LoaiNguoiDung](
	[MaLoai] [int] NOT NULL,
	[ChucVu] [nvarchar](15) NULL,
 CONSTRAINT [PK_ChucVu] PRIMARY KEY CLUSTERED 
(
	[MaLoai] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[LoaiNguoiDung] ([MaLoai], [ChucVu]) VALUES (0, N'Nhân viên')
INSERT [dbo].[LoaiNguoiDung] ([MaLoai], [ChucVu]) VALUES (1, N'Quản lý')
INSERT [dbo].[LoaiNguoiDung] ([MaLoai], [ChucVu]) VALUES (2, N'Admin')
/****** Object:  Table [dbo].[HangHoa]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HangHoa]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HangHoa](
	[MaHangHoa] [int] IDENTITY(1,1) NOT NULL,
	[TenHangHoa] [nvarchar](50) NOT NULL,
	[MaDanhMuc] [int] NULL,
	[MaNhaCungCap] [int] NULL,
	[DonViTinh] [nvarchar](15) NULL,
	[GiaBan] [money] NULL,
	[SoLuong] [int] NULL,
	[TinhTrang] [bit] NULL,
 CONSTRAINT [PK_HangHoa] PRIMARY KEY CLUSTERED 
(
	[MaHangHoa] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[HangHoa] ON
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (1, N'Bim bim', 3, 2, N'túi', 5000.0000, 110, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (2, N'Tập vibook', 5, 4, N'cuốn', 7000.0000, 99, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (3, N'Bột giặt Omo', 6, 3, N'túi', 17000.0000, 99, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (4, N'Bộ xếp hình lego', 1, 1, N'bộ', 120000.0000, 109, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (5, N'Dép lào', 2, 5, N'đôi', 30000.0000, 120, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (6, N'Sữa ông thọ', 3, 6, N'hộp', 20000.0000, 99, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (7, N'Sữa cô gái Hà lan', 3, 6, N'hộp', 25000.0000, 110, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (8, N'Sữa ngôi sao phương nam', 3, 6, N'hộp', 18000.0000, 99, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (9, N'Bột gặt tide', 6, 7, N'túi', 20000.0000, 99, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (10, N'Cốc nhựa 500ml', 6, 5, N'cái', 10000.0000, 99, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (11, N'Quạt nhựa', 6, 5, N'cái', 5000.0000, 99, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (39, N'Cà phê Trung nguyên', 3, 8, N'hộp', 50000.0000, 99, NULL)
INSERT [dbo].[HangHoa] ([MaHangHoa], [TenHangHoa], [MaDanhMuc], [MaNhaCungCap], [DonViTinh], [GiaBan], [SoLuong], [TinhTrang]) VALUES (40, N'thuốc lá 555', NULL, 2, N'hộp', 20000.0000, 0, NULL)
SET IDENTITY_INSERT [dbo].[HangHoa] OFF
/****** Object:  Table [dbo].[NguoiDung]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NguoiDung]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NguoiDung](
	[MaNguoiDung] [int] IDENTITY(1,1) NOT NULL,
	[TaiKhoan] [nvarchar](15) NOT NULL,
	[MatKhau] [nvarchar](50) NOT NULL,
	[TenNguoiDung] [nvarchar](50) NULL,
	[DienThoai] [nvarchar](15) NULL,
	[MaLoai] [int] NULL,
 CONSTRAINT [PK_NguoiDung_1] PRIMARY KEY CLUSTERED 
(
	[MaNguoiDung] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[NguoiDung] ON
INSERT [dbo].[NguoiDung] ([MaNguoiDung], [TaiKhoan], [MatKhau], [TenNguoiDung], [DienThoai], [MaLoai]) VALUES (1, N'admin', N'1234', N'Admin', N'0123', 2)
INSERT [dbo].[NguoiDung] ([MaNguoiDung], [TaiKhoan], [MatKhau], [TenNguoiDung], [DienThoai], [MaLoai]) VALUES (2, N'hanghoa', N'1234', N'Phạm Thị Hóa', N'0980', 1)
INSERT [dbo].[NguoiDung] ([MaNguoiDung], [TaiKhoan], [MatKhau], [TenNguoiDung], [DienThoai], [MaLoai]) VALUES (3, N'kinhdoanh', N'1234', N'Trần Kinh Doanh', N'0122', 0)
INSERT [dbo].[NguoiDung] ([MaNguoiDung], [TaiKhoan], [MatKhau], [TenNguoiDung], [DienThoai], [MaLoai]) VALUES (4, N'user', N'qqqq', N'Nguyễn Ngọc Dùng', N'0165', 0)
INSERT [dbo].[NguoiDung] ([MaNguoiDung], [TaiKhoan], [MatKhau], [TenNguoiDung], [DienThoai], [MaLoai]) VALUES (5, N'aaaa', N'1234', N'aaaa', N'234', 1)
INSERT [dbo].[NguoiDung] ([MaNguoiDung], [TaiKhoan], [MatKhau], [TenNguoiDung], [DienThoai], [MaLoai]) VALUES (6, N'abc', N'1234', N'abc', N'0123456', 0)
SET IDENTITY_INSERT [dbo].[NguoiDung] OFF
/****** Object:  Table [dbo].[PhieuNhapHang]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PhieuNhapHang]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PhieuNhapHang](
	[MaPhieuNhap] [int] IDENTITY(1,1) NOT NULL,
	[MaNhaCungCap] [int] NULL,
	[MaNguoiDung] [int] NULL,
	[NgayLap] [datetime] NULL,
	[TongTien] [money] NULL,
 CONSTRAINT [PK_PhieuNhapHang] PRIMARY KEY CLUSTERED 
(
	[MaPhieuNhap] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[PhieuNhapHang] ON
INSERT [dbo].[PhieuNhapHang] ([MaPhieuNhap], [MaNhaCungCap], [MaNguoiDung], [NgayLap], [TongTien]) VALUES (1, 1, 1, CAST(0x0000A13000000000 AS DateTime), 134000.0000)
INSERT [dbo].[PhieuNhapHang] ([MaPhieuNhap], [MaNhaCungCap], [MaNguoiDung], [NgayLap], [TongTien]) VALUES (3, 2, 4, CAST(0x0000A7A000000000 AS DateTime), 905000.0000)
INSERT [dbo].[PhieuNhapHang] ([MaPhieuNhap], [MaNhaCungCap], [MaNguoiDung], [NgayLap], [TongTien]) VALUES (4, 3, 4, CAST(0x0000A79F00000000 AS DateTime), 0.0000)
INSERT [dbo].[PhieuNhapHang] ([MaPhieuNhap], [MaNhaCungCap], [MaNguoiDung], [NgayLap], [TongTien]) VALUES (5, 4, 3, CAST(0x0000A79F00000000 AS DateTime), 0.0000)
INSERT [dbo].[PhieuNhapHang] ([MaPhieuNhap], [MaNhaCungCap], [MaNguoiDung], [NgayLap], [TongTien]) VALUES (7, 3, 3, CAST(0x0000A7A000000000 AS DateTime), 0.0000)
SET IDENTITY_INSERT [dbo].[PhieuNhapHang] OFF
/****** Object:  Table [dbo].[PhieuDatHang]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PhieuDatHang]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PhieuDatHang](
	[MaPhieuDatHang] [int] IDENTITY(1,1) NOT NULL,
	[MaNhaCungCap] [int] NULL,
	[MaNguoiDung] [int] NULL,
	[NgayLap] [datetime] NULL,
 CONSTRAINT [PK_PhieuDatHang] PRIMARY KEY CLUSTERED 
(
	[MaPhieuDatHang] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PhieuBanHang]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PhieuBanHang]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PhieuBanHang](
	[MaPhieuBanHang] [int] IDENTITY(1,1) NOT NULL,
	[MaNguoiDung] [int] NULL,
	[NgayLap] [datetime] NULL,
	[TongTien] [money] NULL,
 CONSTRAINT [PK_PhieuBanHang] PRIMARY KEY CLUSTERED 
(
	[MaPhieuBanHang] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[PhieuBanHang] ON
INSERT [dbo].[PhieuBanHang] ([MaPhieuBanHang], [MaNguoiDung], [NgayLap], [TongTien]) VALUES (1, 2, CAST(0x0000A79800000000 AS DateTime), 7000.0000)
INSERT [dbo].[PhieuBanHang] ([MaPhieuBanHang], [MaNguoiDung], [NgayLap], [TongTien]) VALUES (2, 1, CAST(0x0000A79D00000000 AS DateTime), 47000.0000)
INSERT [dbo].[PhieuBanHang] ([MaPhieuBanHang], [MaNguoiDung], [NgayLap], [TongTien]) VALUES (3, 4, CAST(0x0000A7A000000000 AS DateTime), 35000.0000)
SET IDENTITY_INSERT [dbo].[PhieuBanHang] OFF
/****** Object:  Table [dbo].[ChiTietPhieuNhap]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuNhap]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChiTietPhieuNhap](
	[MaPhieuNhap] [int] NOT NULL,
	[MaHangHoa] [int] NOT NULL,
	[DonGia] [money] NULL,
	[SoLuong] [int] NULL,
	[ThanhTien] [money] NULL,
 CONSTRAINT [PK_ChiTietPhieuNhap] PRIMARY KEY CLUSTERED 
(
	[MaPhieuNhap] ASC,
	[MaHangHoa] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[ChiTietPhieuNhap] ([MaPhieuNhap], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (1, 2, 7000.0000, 2, 14000.0000)
INSERT [dbo].[ChiTietPhieuNhap] ([MaPhieuNhap], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (1, 4, 12000.0000, 10, 120000.0000)
INSERT [dbo].[ChiTietPhieuNhap] ([MaPhieuNhap], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (3, 1, 5000.0000, 11, 55000.0000)
INSERT [dbo].[ChiTietPhieuNhap] ([MaPhieuNhap], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (3, 5, 30000.0000, 21, 630000.0000)
INSERT [dbo].[ChiTietPhieuNhap] ([MaPhieuNhap], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (3, 7, 20000.0000, 11, 220000.0000)
/****** Object:  Table [dbo].[ChiTietPhieuDatHang]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuDatHang]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChiTietPhieuDatHang](
	[MaPhieuDatHang] [int] NOT NULL,
	[MaHangHoa] [int] NOT NULL,
	[SoLuong] [int] NULL,
 CONSTRAINT [PK_ChiTietPhieuDatHang] PRIMARY KEY CLUSTERED 
(
	[MaPhieuDatHang] ASC,
	[MaHangHoa] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ChiTietPhieuBanHang]    Script Date: 06/29/2017 06:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuBanHang]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChiTietPhieuBanHang](
	[MaPhieuBanHang] [int] NOT NULL,
	[MaHangHoa] [int] NOT NULL,
	[DonGia] [money] NULL,
	[SoLuong] [int] NULL,
	[ThanhTien] [money] NULL,
 CONSTRAINT [PK_ChiTietPhieuBanHang] PRIMARY KEY CLUSTERED 
(
	[MaPhieuBanHang] ASC,
	[MaHangHoa] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[ChiTietPhieuBanHang] ([MaPhieuBanHang], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (1, 2, 7000.0000, 1, 7000.0000)
INSERT [dbo].[ChiTietPhieuBanHang] ([MaPhieuBanHang], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (2, 2, 7000.0000, 1, 7000.0000)
INSERT [dbo].[ChiTietPhieuBanHang] ([MaPhieuBanHang], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (2, 6, 20000.0000, 2, 40000.0000)
INSERT [dbo].[ChiTietPhieuBanHang] ([MaPhieuBanHang], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (3, 1, 5000.0000, 1, 15000.0000)
INSERT [dbo].[ChiTietPhieuBanHang] ([MaPhieuBanHang], [MaHangHoa], [DonGia], [SoLuong], [ThanhTien]) VALUES (3, 5, 30000.0000, 1, 30000.0000)
/****** Object:  ForeignKey [FK_ChiTietPhieuBanHang_HangHoa]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuBanHang_HangHoa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuBanHang]'))
ALTER TABLE [dbo].[ChiTietPhieuBanHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuBanHang_HangHoa] FOREIGN KEY([MaHangHoa])
REFERENCES [dbo].[HangHoa] ([MaHangHoa])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuBanHang_HangHoa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuBanHang]'))
ALTER TABLE [dbo].[ChiTietPhieuBanHang] CHECK CONSTRAINT [FK_ChiTietPhieuBanHang_HangHoa]
GO
/****** Object:  ForeignKey [FK_ChiTietPhieuBanHang_PhieuBanHang]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuBanHang_PhieuBanHang]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuBanHang]'))
ALTER TABLE [dbo].[ChiTietPhieuBanHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuBanHang_PhieuBanHang] FOREIGN KEY([MaPhieuBanHang])
REFERENCES [dbo].[PhieuBanHang] ([MaPhieuBanHang])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuBanHang_PhieuBanHang]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuBanHang]'))
ALTER TABLE [dbo].[ChiTietPhieuBanHang] CHECK CONSTRAINT [FK_ChiTietPhieuBanHang_PhieuBanHang]
GO
/****** Object:  ForeignKey [FK_ChiTietPhieuDatHang_HangHoa]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuDatHang_HangHoa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuDatHang]'))
ALTER TABLE [dbo].[ChiTietPhieuDatHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuDatHang_HangHoa] FOREIGN KEY([MaHangHoa])
REFERENCES [dbo].[HangHoa] ([MaHangHoa])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuDatHang_HangHoa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuDatHang]'))
ALTER TABLE [dbo].[ChiTietPhieuDatHang] CHECK CONSTRAINT [FK_ChiTietPhieuDatHang_HangHoa]
GO
/****** Object:  ForeignKey [FK_ChiTietPhieuDatHang_PhieuDatHang]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuDatHang_PhieuDatHang]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuDatHang]'))
ALTER TABLE [dbo].[ChiTietPhieuDatHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuDatHang_PhieuDatHang] FOREIGN KEY([MaPhieuDatHang])
REFERENCES [dbo].[PhieuDatHang] ([MaPhieuDatHang])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuDatHang_PhieuDatHang]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuDatHang]'))
ALTER TABLE [dbo].[ChiTietPhieuDatHang] CHECK CONSTRAINT [FK_ChiTietPhieuDatHang_PhieuDatHang]
GO
/****** Object:  ForeignKey [FK_ChiTietPhieuNhap_HangHoa]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuNhap_HangHoa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuNhap]'))
ALTER TABLE [dbo].[ChiTietPhieuNhap]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuNhap_HangHoa] FOREIGN KEY([MaHangHoa])
REFERENCES [dbo].[HangHoa] ([MaHangHoa])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuNhap_HangHoa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuNhap]'))
ALTER TABLE [dbo].[ChiTietPhieuNhap] CHECK CONSTRAINT [FK_ChiTietPhieuNhap_HangHoa]
GO
/****** Object:  ForeignKey [FK_ChiTietPhieuNhap_PhieuNhapHang]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuNhap_PhieuNhapHang]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuNhap]'))
ALTER TABLE [dbo].[ChiTietPhieuNhap]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuNhap_PhieuNhapHang] FOREIGN KEY([MaPhieuNhap])
REFERENCES [dbo].[PhieuNhapHang] ([MaPhieuNhap])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChiTietPhieuNhap_PhieuNhapHang]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChiTietPhieuNhap]'))
ALTER TABLE [dbo].[ChiTietPhieuNhap] CHECK CONSTRAINT [FK_ChiTietPhieuNhap_PhieuNhapHang]
GO
/****** Object:  ForeignKey [FK_HangHoa_DanhMuc]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HangHoa_DanhMuc]') AND parent_object_id = OBJECT_ID(N'[dbo].[HangHoa]'))
ALTER TABLE [dbo].[HangHoa]  WITH CHECK ADD  CONSTRAINT [FK_HangHoa_DanhMuc] FOREIGN KEY([MaDanhMuc])
REFERENCES [dbo].[DanhMuc] ([MaDanhMuc])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HangHoa_DanhMuc]') AND parent_object_id = OBJECT_ID(N'[dbo].[HangHoa]'))
ALTER TABLE [dbo].[HangHoa] CHECK CONSTRAINT [FK_HangHoa_DanhMuc]
GO
/****** Object:  ForeignKey [FK_HangHoa_NhaCungCap]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HangHoa_NhaCungCap]') AND parent_object_id = OBJECT_ID(N'[dbo].[HangHoa]'))
ALTER TABLE [dbo].[HangHoa]  WITH CHECK ADD  CONSTRAINT [FK_HangHoa_NhaCungCap] FOREIGN KEY([MaNhaCungCap])
REFERENCES [dbo].[NhaCungCap] ([MaNhaCungCap])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HangHoa_NhaCungCap]') AND parent_object_id = OBJECT_ID(N'[dbo].[HangHoa]'))
ALTER TABLE [dbo].[HangHoa] CHECK CONSTRAINT [FK_HangHoa_NhaCungCap]
GO
/****** Object:  ForeignKey [FK_NguoiDung_LoaiNguoiDung]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NguoiDung_LoaiNguoiDung]') AND parent_object_id = OBJECT_ID(N'[dbo].[NguoiDung]'))
ALTER TABLE [dbo].[NguoiDung]  WITH CHECK ADD  CONSTRAINT [FK_NguoiDung_LoaiNguoiDung] FOREIGN KEY([MaLoai])
REFERENCES [dbo].[LoaiNguoiDung] ([MaLoai])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NguoiDung_LoaiNguoiDung]') AND parent_object_id = OBJECT_ID(N'[dbo].[NguoiDung]'))
ALTER TABLE [dbo].[NguoiDung] CHECK CONSTRAINT [FK_NguoiDung_LoaiNguoiDung]
GO
/****** Object:  ForeignKey [FK_PhieuBanHang_NguoiDung]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuBanHang_NguoiDung]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuBanHang]'))
ALTER TABLE [dbo].[PhieuBanHang]  WITH CHECK ADD  CONSTRAINT [FK_PhieuBanHang_NguoiDung] FOREIGN KEY([MaNguoiDung])
REFERENCES [dbo].[NguoiDung] ([MaNguoiDung])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuBanHang_NguoiDung]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuBanHang]'))
ALTER TABLE [dbo].[PhieuBanHang] CHECK CONSTRAINT [FK_PhieuBanHang_NguoiDung]
GO
/****** Object:  ForeignKey [FK_PhieuDatHang_NguoiDung]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDatHang_NguoiDung]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDatHang]'))
ALTER TABLE [dbo].[PhieuDatHang]  WITH CHECK ADD  CONSTRAINT [FK_PhieuDatHang_NguoiDung] FOREIGN KEY([MaNguoiDung])
REFERENCES [dbo].[NguoiDung] ([MaNguoiDung])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDatHang_NguoiDung]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDatHang]'))
ALTER TABLE [dbo].[PhieuDatHang] CHECK CONSTRAINT [FK_PhieuDatHang_NguoiDung]
GO
/****** Object:  ForeignKey [FK_PhieuNhapHang_NguoiDung]    Script Date: 06/29/2017 06:52:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuNhapHang_NguoiDung]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuNhapHang]'))
ALTER TABLE [dbo].[PhieuNhapHang]  WITH CHECK ADD  CONSTRAINT [FK_PhieuNhapHang_NguoiDung] FOREIGN KEY([MaNguoiDung])
REFERENCES [dbo].[NguoiDung] ([MaNguoiDung])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuNhapHang_NguoiDung]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuNhapHang]'))
ALTER TABLE [dbo].[PhieuNhapHang] CHECK CONSTRAINT [FK_PhieuNhapHang_NguoiDung]
GO
