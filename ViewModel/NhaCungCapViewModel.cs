﻿using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModel.Utilities;

namespace ViewModel
{
    public class NhaCungCapViewModel : DataViewModel
    {
        public NhaCungCapViewModel()
        {
            base.Header = "Nhà cung cấp";
        }
        private ObservableCollection<NhaCungCap> listData;
        public ObservableCollection<NhaCungCap> ListData
        {
            get
            {
                if (listData == null)
                {
                    listData = new ObservableCollection<NhaCungCap>(db.NhaCungCaps.ToList());
                }
                return listData;
            }
            set
            {
                if (listData != value)
                {
                    listData = value; OnPropertyChanged("ListData");
                }
            }
        }

        //lưu nhà cung cấp khi thêm mới
        private ICommand saveCreate;
        public ICommand SaveCreate
        {
            get
            {
                if (saveCreate == null)
                {
                    saveCreate = new RelayCommand<DataGrid>(
                        (m) =>
                        {
                            ObservableCollection<DataGridInput> datagrid = (ObservableCollection<DataGridInput>)m.ItemsSource;
                            var list = new List<NhaCungCap>();
                            (datagrid.Where(c => c.Column1 != "").ToList()).ForEach<DataGridInput>(k =>
                            {
                                list.Add(new NhaCungCap
                                {
                                    TenNhaCungCap = k.Column1,
                                    DiaChi = k.Column2,
                                    DienThoai = k.Column3
                                });
                            });
                            ListData.AddAll<NhaCungCap>(list);
                            db.NhaCungCaps.InsertAll<NhaCungCap>(list);
                            db.SaveChanges();
                            MessageBox.Show("Đã thêm:" + " " + list.Count().ToString() + " " + "Nhà cung cấp");
                            ClearGrid();
                        }
                        );
                }
                return saveCreate;
            }
        }
     //xóa nhà cung cấp
        private ICommand delete;
        public ICommand Delete
        {
            get
            {
                delete = new RelayCommand<DataGrid>((m) =>
                {
                    MessageBoxResult msg = MessageBox.Show("Chắc chưa?", "Xác nhận xóa", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (msg == MessageBoxResult.Yes)
                    {
                        List<NhaCungCap> list = (m.SelectedItems as IList).ToList<NhaCungCap>();
                        ListData.RemoveAll<NhaCungCap>(list);
                        db.NhaCungCaps.RemoveAll<NhaCungCap>(list);
                        db.SaveChanges();
                    }
                      else
                    {
                        return;
                    }
                });
                return delete;
            }
        }
    }
}
