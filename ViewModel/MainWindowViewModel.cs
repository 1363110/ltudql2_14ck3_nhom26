﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModel.Utilities;

namespace ViewModel
{
    public class MainWindowViewModel : MainWindowViewModelBase
    {
        
        private ICommand icDanhMuc;
        public ICommand IcDanhMuc
        {
            get
            {
                if (icDanhMuc == null)
                {
                   icDanhMuc = new RelayCommand(() => { AddViewModel<DanhMucViewModel>(); });
                }
                return icDanhMuc;
            }
        }
        //
        private ICommand icNhaCungCap;
        public ICommand IcNhaCungCap
        {
            get
            {
                if (icNhaCungCap == null)
                {
                    icNhaCungCap = new RelayCommand(() => { AddViewModel<NhaCungCapViewModel>(); });
                }
                return icNhaCungCap;
            }
        }
        //
        private ICommand icSanPham;
        public ICommand IcSanPham
        {
            get
            {
                if (icSanPham == null)
                {
                    icSanPham = new RelayCommand(() => { AddViewModel<SanPhamViewModel>();});
                }
                return icSanPham;
            }
        }
        //
        private ICommand icTimSanPham;
        public ICommand IcTimSanPham
        {
            get
            {
                if (icTimSanPham == null)
                {
                    icTimSanPham = new RelayCommand(() => { AddViewModel<TimSanPhamViewModel>(); });
                }
                return icTimSanPham;
            }
        }
        //
        private ICommand icBanHang;
        public ICommand IcBanHang
        {
            get
            {
                if (icBanHang == null)
                {
                    icBanHang = new RelayCommand(() => { AddViewModel<BanHangViewModel>(); });
                }
                return icBanHang;
            }
        }
        //
        private ICommand icNhapHang;
        public ICommand IcNhapHang
        {
            get
            {
                if (icNhapHang == null)
                {
                    icNhapHang = new RelayCommand(() => { AddViewModel<NhapHangViewModel>(); });
                }
                return icNhapHang;
            }
        }
        //
        private ICommand icNhanVien;
        public ICommand IcNhanVien
        {
            get
            {
                if (icNhanVien == null)
                {
                    icNhanVien = new RelayCommand(() => { AddViewModel<NhanVienViewModel>(); });
                }
                return icNhanVien;
            }
        }

    }
}
