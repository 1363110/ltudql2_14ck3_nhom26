﻿using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModel.Utilities;

namespace ViewModel
{
    public class SanPhamViewModel : DataViewModel
    {
        public SanPhamViewModel()
        {
            base.Header = "Sản phẩm";
            Load();
        }
        //Làm mới dữ liệu từ database
        private void Load()
        {
            CurPage = 1;
            int totalPage;
            ListData = listPage(CurPage, pageSize, out totalPage);
            TotalPage = totalPage;
        }
        //load dữ liệu + phân trang
        ObservableCollection<HangHoa> listPage(int page, int pageSize, out int totalPage)
        {
            totalPage = (int)Math.Ceiling(db.HangHoas.Count() * 1.0 / pageSize);
            var list = new ObservableCollection<HangHoa>(db.HangHoas.OrderBy(p => p.MaHangHoa).Skip((page - 1) * pageSize).Take(pageSize).ToList());
            return list;
        }

        #region--Binding--
        //Danh sách Danh mục trong Combobox
        private Dictionary<string, string> listDanhMuc;
        public Dictionary<string, string> ListDanhMuc
        {
            get
            {
                var list = new Dictionary<string, string>();

                db.DanhMucs.ForEach<DanhMuc>(m =>
                {
                    list.Add(m.MaDanhMuc.ToString(), m.TenDanhMuc);
                });
                list.Add("", "chưa xác định");
                listDanhMuc = list;
                return listDanhMuc;
            }
        }

        //binding Danh sách dữ liệu
        private ObservableCollection<HangHoa> listData;
        public ObservableCollection<HangHoa> ListData
        {
            get
            {
                if (listData == null)
                {
                    listData = new ObservableCollection<HangHoa>();
                }
                return listData;
            }
            set
            {
                if (listData != value)
                {
                    listData = value; OnPropertyChanged("ListData");
                }
            }
        }
        #endregion

        #region--Command--
        //xử lý lưu sản phẩm khi thêm mới
        private ICommand saveCreate;
        public ICommand SaveCreate
        {
            get
            {
                if (saveCreate == null)
                {
                    saveCreate = new RelayCommand<DataGrid>(
                        (m) =>
                        {
                            ObservableCollection<DataGridInput> datagrid = (ObservableCollection<DataGridInput>)m.ItemsSource;
                            var list = new List<HangHoa>();
                            (datagrid.Where(c => c.Column1 != "").ToList()).ForEach<DataGridInput>(k =>
                            {
                                if (k.Column6 == "")
                                {
                                    k.Column6 = "0";
                                }
                                if (k.Column5 == "")
                                {
                                    k.Column5 = "0";
                                }
                                list.Add(new HangHoa
                                {
                                    TenHangHoa = k.Column1,
                                    MaDanhMuc = k.Column2.ToInt(),
                                    MaNhaCungCap = k.Column3.ToInt(),
                                    DonViTinh = k.Column4,
                                    GiaBan = k.Column5.ToDecimal(),
                                    SoLuong = k.Column6.ToInt()
                                });
                            });
                            ListData.AddAll<HangHoa>(list);
                            db.HangHoas.InsertAll<HangHoa>(list);
                            db.SaveChanges();
                            MessageBox.Show("Đã thêm:" + " " + list.Count().ToString() + " " + "Sản phẩm");
                            ClearGrid();
                            Load();
                        }
                        );
                }
                return saveCreate;
            }
        }
        //xóa sản phẩm
        private ICommand delete;
        public ICommand Delete
        {
            get
            {
                delete = new RelayCommand<DataGrid>((m) =>
                {
                    MessageBoxResult msg = MessageBox.Show("Chắc chưa?", "Xác nhận xóa", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (msg == MessageBoxResult.Yes)
                    {
                        List<HangHoa> list = (m.SelectedItems as IList).ToList<HangHoa>();
                        ListData.RemoveAll<HangHoa>(list);
                        db.HangHoas.RemoveAll<HangHoa>(list);
                        db.SaveChanges();
                        Load();
                    }
                    else
                    {
                        return;
                    }
                });
                return delete;
            }
        }
        //Xử lý phân trang
        private ICommand icNext;
        public ICommand IcNext
        {
            get
            {
                if (icNext == null)
                {
                    icNext = new RelayCommand(() =>
                    {
                        if (CurPage < TotalPage)
                        {
                            int totalPage;
                            CurPage++;
                            ListData = listPage(CurPage, pageSize, out totalPage);
                        }
                    });
                }
                return icNext;
            }
        }
        //
        private ICommand icPreview;
        public ICommand IcPreview
        {
            get
            {
                if (icPreview == null)
                {
                    icPreview = new RelayCommand(() =>
                    {
                        if (CurPage > 1)
                        {
                            int totalPage;
                            CurPage--;
                            ListData = listPage(CurPage, pageSize, out totalPage);
                        }
                    });
                }
                return icPreview;
            }
        }
        #endregion

    }
}
