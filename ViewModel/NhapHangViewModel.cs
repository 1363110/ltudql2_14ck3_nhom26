﻿using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModel.Utilities;

namespace ViewModel
{
    public class NhapHangViewModel : DataViewModel
    {
        public NhapHangViewModel()
        {
            base.Header = "Nhập hàng";
        }
        #region--Binding--
        private ObservableCollection<PhieuNhapHang> listData;
        public ObservableCollection<PhieuNhapHang> ListData
        {
            get
            {
                if (listData == null)
                {
                    listData = new ObservableCollection<PhieuNhapHang>(db.PhieuNhapHangs.ToList());
                }
                return listData;
            }
            set
            {
                if (listData != value)
                {
                    listData = value; OnPropertyChanged("ListData");
                }
            }
        }
        //
        private ObservableCollection<ChiTietPhieuNhap> listDataCT;
        public ObservableCollection<ChiTietPhieuNhap> ListDataCT
        {
            get
            {
                if (listDataCT == null)
                {
                    listDataCT = new ObservableCollection<ChiTietPhieuNhap>();
                }
                return listDataCT;
            }
            set
            {
                if (listDataCT != value)
                {
                    listDataCT = value; OnPropertyChanged("ListDataCT");
                }
            }
        }
        //
        private PhieuNhapHang pnh;
        public PhieuNhapHang PNH
        {
            get
            {
                if (pnh == null)
                {
                    pnh = new PhieuNhapHang();
                }
                return pnh;
            }
            set
            {
                if (pnh != value)
                {
                    pnh = value; OnPropertyChanged("PNH");
                }

            }
        }
        //
        private ChiTietPhieuNhap ctpnh;
        public ChiTietPhieuNhap CTPNH
        {
            get
            {
                if (ctpnh == null)
                {
                    ctpnh = new ChiTietPhieuNhap();
                }
                return ctpnh;
            }
            set
            {
                if (ctpnh != value)
                {
                    ctpnh = value; OnPropertyChanged("CTPNH");
                }

            }
        }
        //
        private HangHoa hh;
        public HangHoa HH
        {
            get
            {
                if (hh == null)
                {
                    hh = new HangHoa();
                }
                return hh;
            }
            set
            {
                if (hh != value)
                {
                    hh = value; OnPropertyChanged("HH");
                }

            }
        }
        #endregion

        #region--Command--
        //lưu phiếu nhập khi thêm mới
        private ICommand saveCreate;
        public ICommand SaveCreate
        {
            get
            {
                if (saveCreate == null)
                {
                    saveCreate = new RelayCommand<DataGrid>(
                        (m) =>
                        {
                            ObservableCollection<DataGridInput> datagrid = (ObservableCollection<DataGridInput>)m.ItemsSource;
                            var list = new List<PhieuNhapHang>();
                            (datagrid.Where(c => c.Column1 != "" && c.Column2 != "").ToList()).ForEach<DataGridInput>(k =>
                              {
                                  list.Add(new PhieuNhapHang
                                  {
                                      MaNhaCungCap = k.Column1.ToInt(),
                                      MaNguoiDung = k.Column2.ToInt(),
                                      NgayLap = DateTime.Today,
                                      TongTien = 0
                                  });
                              });
                            ListData.AddAll<PhieuNhapHang>(list);
                            db.PhieuNhapHangs.InsertAll<PhieuNhapHang>(list);
                            db.SaveChanges();
                            MessageBox.Show("Đã thêm:" + " " + list.Count().ToString() + " " + "Phiếu nhập");
                            ClearGrid();
                        }
                        );
                }
                return saveCreate;
            }
        }
        //xóa phiếu nhập
        private ICommand delete;
        public ICommand Delete
        {
            get
            {
                delete = new RelayCommand<DataGrid>((m) =>
                {
                    MessageBoxResult msg = MessageBox.Show("Chắc chưa?", "Xác nhận xóa", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (msg == MessageBoxResult.Yes)
                    {
                        List<PhieuNhapHang> list = (m.SelectedItems as IList).ToList<PhieuNhapHang>();
                        ListData.RemoveAll<PhieuNhapHang>(list);
                        db.PhieuNhapHangs.RemoveAll<PhieuNhapHang>(list);
                        db.SaveChanges();
                    }
                    else
                    {
                        return;
                    }
                });
                return delete;
            }
        }
        //xem chi tiết phiếu
        private ICommand detail;
        public ICommand Detail
        {
            get
            {
                if (detail == null)
                {
                    detail = new RelayCommand<DataGrid>((m) =>
                    {
                        PhieuNhapHang p = (PhieuNhapHang)m.SelectedItem;
                        PNH = p;
                        ListDataCT = new ObservableCollection<ChiTietPhieuNhap>(db.ChiTietPhieuNhaps.Where(x => x.MaPhieuNhap == p.MaPhieuNhap).ToList());
                        IndexView = false; AddView = false; EditView = false; DetailView = true;
                    });
                }
                return detail;
            }

        }
        //lưu chi tiết phiếu khi thêm mới
        private ICommand saveCreateCT;
        public ICommand SaveCreateCT
        {
            get
            {
                if (saveCreateCT == null)
                {
                    saveCreateCT = new RelayCommand<DataGrid>(
                        (m) =>
                        {
                            string maphieu = PNH.MaPhieuNhap.ToString();
                            ObservableCollection<DataGridInput> datagrid = (ObservableCollection<DataGridInput>)m.ItemsSource;
                            var list = new List<ChiTietPhieuNhap>();
                            (datagrid.Where(c => c.Column2 != "" && c.Column3 != "" && c.Column4 != "").ToList()).ForEach<DataGridInput>(k =>
                                {
                                    k.Column1 = maphieu;
                                    int? masp = k.Column2.ToInt();
                                    decimal? thanhtien = k.Column3.ToDecimal() * k.Column4.ToInt();
                                    k.Column5 = thanhtien.ToString();
                                    //kiểm tra sản phẩm đã có trong đơn nhập rồi thỉ bỏ qua, chỉ thêm sản phẩm mới
                                    int sl = ListDataCT.Where(x => x.MaHangHoa == masp).Count();
                                    if (sl < 1)
                                    {
                                        CTPNH = new ChiTietPhieuNhap
                                        {
                                            MaPhieuNhap = int.Parse(k.Column1),
                                            MaHangHoa = int.Parse(k.Column2),
                                            DonGia = k.Column3.ToDecimal(),
                                            SoLuong = k.Column4.ToInt(),
                                            ThanhTien = thanhtien,
                                        };
                                        HH = db.HangHoas.Find(CTPNH.MaHangHoa);
                                        HH.SoLuong += CTPNH.SoLuong;
                                        db.Entry(HH).State = EntityState.Modified;//cập nhật số lượng sản phẩm
                                        list.Add(CTPNH);
                                    }
                                });
                            ListDataCT.AddAll<ChiTietPhieuNhap>(list);
                            var sum = ListDataCT.Sum(a => a.ThanhTien);
                            PNH.TongTien = sum;
                            db.ChiTietPhieuNhaps.InsertAll<ChiTietPhieuNhap>(list);
                            db.Entry(PNH).State = EntityState.Modified; //cập nhật tổng tiền
                            db.SaveChanges();
                            MessageBox.Show("Đã thêm vào:" + " " + list.Count().ToString() + " " + "Sản phẩm");
                            ClearGrid();
                            ListData = new ObservableCollection<PhieuNhapHang>(db.PhieuNhapHangs.ToList());
                        });
                }
                return saveCreateCT;
            }
        }
        //xóa sản phẩm khỏi phiếu nhập
        private ICommand deleteCT;
        public ICommand DeleteCT
        {
            get
            {
                deleteCT = new RelayCommand<DataGrid>((m) =>
                {
                    MessageBoxResult msg = MessageBox.Show("Chắc chưa?", "Xác nhận xóa", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (msg == MessageBoxResult.Yes)
                    {
                        List<ChiTietPhieuNhap> list = (m.SelectedItems as IList).ToList<ChiTietPhieuNhap>();
                        ListDataCT.RemoveAll<ChiTietPhieuNhap>(list);
                        var sum = ListDataCT.Sum(a => a.ThanhTien);
                        if (sum == null) { sum = 0; }
                        PNH.TongTien = sum;
                        db.ChiTietPhieuNhaps.RemoveAll<ChiTietPhieuNhap>(list);
                        db.Entry(PNH).State = EntityState.Modified;
                        db.SaveChanges();
                        ListData = new ObservableCollection<PhieuNhapHang>(db.PhieuNhapHangs.ToList());
                    }
                    else
                    {
                        return;
                    }
                });
                return deleteCT;
            }
        }
        #endregion
    }
}
