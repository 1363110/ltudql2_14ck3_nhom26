﻿using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModel.Utilities;
namespace ViewModel
{
    public class BanHangViewModel : DataViewModel
    {
        public BanHangViewModel()
        {
            base.Header = "Phiếu bán hàng";
        }

        #region--Binding--
        private ObservableCollection<PhieuBanHang> listData;
        public ObservableCollection<PhieuBanHang> ListData
        {
            get
            {
                if (listData == null)
                {
                    listData = new ObservableCollection<PhieuBanHang>(db.PhieuBanHangs.ToList());
                }
                return listData;
            }
            set
            {
                if (listData != value)
                {
                    listData = value; OnPropertyChanged("ListData");
                }
            }
        }
        //
        private ObservableCollection<ChiTietPhieuBanHang> listDataCT;
        public ObservableCollection<ChiTietPhieuBanHang> ListDataCT
        {
            get
            {
                if (listDataCT == null)
                {
                    listDataCT = new ObservableCollection<ChiTietPhieuBanHang>();
                }
                return listDataCT;
            }
            set
            {
                if (listDataCT != value)
                {
                    listDataCT = value; OnPropertyChanged("ListDataCT");
                }
            }
        }
        //
        private PhieuBanHang pbh;
        public PhieuBanHang PBH
        {
            get
            {
                if (pbh == null)
                {
                    pbh = new PhieuBanHang();
                }
                return pbh;
            }
            set
            {
                if (pbh != value)
                {
                    pbh = value; OnPropertyChanged("PBH");
                }

            }
        }
        //
        private ChiTietPhieuBanHang ctpbh;
        public ChiTietPhieuBanHang CTPBH
        {
            get
            {
                if (ctpbh == null)
                {
                    ctpbh = new ChiTietPhieuBanHang();
                }
                return ctpbh;
            }
            set
            {
                if (ctpbh != value)
                {
                    ctpbh = value; OnPropertyChanged("CTPBH");
                }

            }
        }
        //
        private HangHoa hh;
        public HangHoa HH
        {
            get
            {
                if (hh == null)
                {
                    hh = new HangHoa();
                }
                return hh;
            }
            set
            {
                if (hh != value)
                {
                    hh = value; OnPropertyChanged("HH");
                }

            }
        }
    
        //binding danh sách phản phẩm sử dụng cho combobox
        private ObservableCollection<HangHoa> listHangHoa;
        public ObservableCollection<HangHoa> ListHangHoa
        {
            get
            {
                if (listHangHoa == null)
                {
                    listHangHoa = new ObservableCollection<HangHoa>(db.HangHoas.ToList());
                }
                return listHangHoa;
            }
            set
            {
                if (listHangHoa != value)
                {
                    listHangHoa = value; OnPropertyChanged("ListHangHoa");
                }
            }
        }
        #endregion

        #region --xử lý lại số dòng của table excel nhập liệu--
        //Thuộc tính Binding tạo Table DataGrid
        private ObservableCollection<DataGridInput> dataGridBH;
        public ObservableCollection<DataGridInput> DataGridBH
        {
            get
            {
                if (dataGridBH == null)
                {
                    dataGridBH = DataGridInput.GetTable(1);

                }
                return dataGridBH;
            }
            set
            {
                if (dataGridBH != value)
                {
                    dataGridBH = value; OnPropertyChanged("DataGridBH");
                }

            }
        }
        //Xóa dữ liệu ô nhập
        public void ClearGridBH()
        {
            DataGridBH.Clear();
            DataGridBH = DataGridInput.GetTable(1);
        }
        //xử lý clear dữ liệu DataGrid
        private ICommand clearCreateBH;
        public ICommand ClearCreateBH
        {
            get
            {
                clearCreateBH = new RelayCommand(() => { ClearGridBH(); });
                return clearCreateBH;
            }
        }
        #endregion

        #region--Command--
        //lưu phiếu khi thêm mới
        private ICommand saveCreate;
        public ICommand SaveCreate
        {
            get
            {
                if (saveCreate == null)
                {
                    saveCreate = new RelayCommand<DataGrid>(
                        (m) =>
                        {
                            ObservableCollection<DataGridInput> datagrid = (ObservableCollection<DataGridInput>)m.ItemsSource;
                            var list = new List<PhieuBanHang>();
                            (datagrid.Where(c => c.Column1 != "").ToList()).ForEach<DataGridInput>(k =>
                            {
                                k.Column3 = "0";
                                list.Add(new PhieuBanHang
                                {
                                    MaNguoiDung = k.Column1.ToInt(),
                                    NgayLap = k.Column2.ToDateTime(),
                                    TongTien = k.Column3.ToDecimal()
                                });
                            });
                            ListData.AddAll<PhieuBanHang>(list);
                            db.PhieuBanHangs.InsertAll<PhieuBanHang>(list);
                            db.SaveChanges();
                            MessageBox.Show("Đã thêm:" + " " + list.Count().ToString() + " " + "phiếu bán hàng");
                            ClearGrid();
                        }
                        );
                }
                return saveCreate;
            }
        }
        //xóa phiếu
        private ICommand delete;
        public ICommand Delete
        {
            get
            {
                delete = new RelayCommand<DataGrid>((m) =>
                {
                    MessageBoxResult msg = MessageBox.Show("Chắc chưa?", "Xác nhận xóa", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (msg == MessageBoxResult.Yes)
                    {
                        List<PhieuBanHang> list = (m.SelectedItems as IList).ToList<PhieuBanHang>();
                        ListData.RemoveAll<PhieuBanHang>(list);
                        db.PhieuBanHangs.RemoveAll<PhieuBanHang>(list);
                        db.SaveChanges();
                    }
                    else
                    {
                        return;
                    }
                });
                return delete;
            }
        }

        //xem chi tiết phiếu
        private ICommand detail;
        public ICommand Detail
        {
            get
            {
                if (detail == null)
                {
                    detail = new RelayCommand<DataGrid>((m) =>
                    {
                        PhieuBanHang p = (PhieuBanHang)m.SelectedItem;
                        PBH = p;
                        ListDataCT = new ObservableCollection<ChiTietPhieuBanHang>(db.ChiTietPhieuBanHangs.Where(x => x.MaPhieuBanHang == p.MaPhieuBanHang).ToList());
                        IndexView = false; AddView = false; EditView = false; DetailView = true;
                    });
                }
                return detail;
            }
        }
        //lưu chi tiết phiếu khi thêm mới
        private ICommand saveCreateCT;
        public ICommand SaveCreateCT
        {
            get
            {
                if (saveCreateCT == null)
                {
                    saveCreateCT = new RelayCommand<DataGrid>(
                        (m) =>
                        {
                            string maphieu = PBH.MaPhieuBanHang.ToString();
                            ObservableCollection<DataGridInput> datagrid = (ObservableCollection<DataGridInput>)m.ItemsSource;
                            var list = new List<ChiTietPhieuBanHang>();
                            (datagrid.Where(c => c.Column2 != "").ToList()).ForEach<DataGridInput>(k =>
                               {
                                   k.Column1 = maphieu;
                                   int? masp = k.Column2.ToInt();
                                   k.Column3 = db.HangHoas.SingleOrDefault(x => x.MaHangHoa == masp).GiaBan.ToString();
                                   decimal? thanhtien = k.Column3.ToDecimal() * k.Column4.ToInt();
                                   k.Column5 = thanhtien.ToString();
                                   //kiểm tra sản phẩm đã có trong đơn hàng rồi thỉ bỏ qua, chỉ thêm sản phẩm mới
                                   int sl = ListDataCT.Where(x => x.MaHangHoa == masp).Count();

                                   if (sl < 1)
                                   {
                                       CTPBH = new ChiTietPhieuBanHang
                                       {
                                           MaPhieuBanHang = int.Parse(k.Column1),
                                           MaHangHoa = int.Parse(k.Column2),
                                           DonGia = k.Column3.ToDecimal(),
                                           SoLuong = k.Column4.ToInt(),
                                           ThanhTien = k.Column5.ToDecimal()
                                       };
                                       HH = db.HangHoas.Find(CTPBH.MaHangHoa);
                                       if (HH.SoLuong >= CTPBH.SoLuong)
                                       {
                                           HH.SoLuong -= CTPBH.SoLuong;
                                       }
                                       else
                                       {
                                           //MessageBox.Show("Không đủ hàng, Chỉ còn: "+HH.SoLuong.ToString()+" sản phẩm");
                                           return;
                                       }
                                       db.Entry(HH).State = EntityState.Modified;//cập nhật số lượng sản phẩm
                                       list.Add(CTPBH);
                                   }
                               });
                            ListDataCT.AddAll<ChiTietPhieuBanHang>(list);
                            var sum = ListDataCT.Sum(a => a.ThanhTien);
                            PBH.TongTien = sum;
                            db.ChiTietPhieuBanHangs.InsertAll<ChiTietPhieuBanHang>(list);
                            db.Entry(PBH).State = EntityState.Modified; //cập nhật tổng tiền
                            db.SaveChanges();
                            MessageBox.Show("Đã thêm vào:" + " " + list.Count().ToString() + " " + "Sản phẩm");
                            ClearGrid();
                            ListData = new ObservableCollection<PhieuBanHang>(db.PhieuBanHangs.ToList());
                        }
                        );
                }
                return saveCreateCT;
            }
        }
        //xóa sản phẩm khỏi phiếu bán
        private ICommand deleteCT;
        public ICommand DeleteCT
        {
            get
            {
                deleteCT = new RelayCommand<DataGrid>((m) =>
                {
                    MessageBoxResult msg = MessageBox.Show("Chắc chưa?", "Xác nhận xóa", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (msg == MessageBoxResult.Yes)
                    {
                        List<ChiTietPhieuBanHang> list = (m.SelectedItems as IList).ToList<ChiTietPhieuBanHang>();
                        ListDataCT.RemoveAll<ChiTietPhieuBanHang>(list);
                        var sum = ListDataCT.Sum(a => a.ThanhTien);
                        if (sum == null) { sum = 0; }
                        PBH.TongTien = sum;
                        db.ChiTietPhieuBanHangs.RemoveAll<ChiTietPhieuBanHang>(list);
                        db.Entry(PBH).State = EntityState.Modified;
                        db.SaveChanges();
                        ListData = new ObservableCollection<PhieuBanHang>(db.PhieuBanHangs.ToList());
                    }
                    else
                    {
                        return;
                    }
                });
                return deleteCT;
            }
        }

        //lưu tất cả khi sửa chi tiết
        private ICommand saveAllBH;
        public ICommand SaveAllBH
        {
            get
            {
                if (saveAllBH == null)
                {
                    saveAllBH = new RelayCommand(() =>
                    {
                        //ListDataCT.ThanhTien
                        MessageBox.Show("Đã cập nhật");
                        db.SaveChanges();
                        //ListData = ListData = new ObservableCollection<PhieuBanHang>(db.PhieuBanHangs.ToList());
                    });
                }
                return saveAllBH;
            }
        }
        #endregion

    }
}
