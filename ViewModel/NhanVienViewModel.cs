﻿using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModel.Utilities;

namespace ViewModel
{
    public class NhanVienViewModel : DataViewModel
    {
        public NhanVienViewModel()
        {
            base.Header = "Tài khoản người dùng";
        }
        private ObservableCollection<NguoiDung> listData;
        public ObservableCollection<NguoiDung> ListData
        {
            get
            {
                if (listData == null)
                {
                    listData = new ObservableCollection<NguoiDung>(db.NguoiDungs.ToList());
                }
                return listData;
            }
            set
            {
                if (listData != value)
                {
                    listData = value; OnPropertyChanged("ListData");
                }
            }
        }

        //lưu người dùng khi thêm mới
        private ICommand saveCreate;
        public ICommand SaveCreate
        {
            get
            {
                if (saveCreate == null)
                {
                    saveCreate = new RelayCommand<DataGrid>(
                        (m) =>
                        {
                            ObservableCollection<DataGridInput> datagrid = (ObservableCollection<DataGridInput>)m.ItemsSource;
                            var list = new List<NguoiDung>();
                            (datagrid.Where(c => c.Column1 != "" && c.Column2 != "").ToList()).ForEach<DataGridInput>(k =>
                            {
                                list.Add(new NguoiDung
                                {
                                    TaiKhoan = k.Column1,
                                    MatKhau = k.Column2,
                                    TenNguoiDung = k.Column3,
                                    DienThoai = k.Column4, 
                                    MaLoai = 0
                                });
                            });
                            ListData.AddAll<NguoiDung>(list);
                            db.NguoiDungs.InsertAll<NguoiDung>(list);
                            db.SaveChanges();
                            MessageBox.Show("Đã thêm:" + " " + list.Count().ToString() + " " + "Người dùng");
                            ClearGrid();
                        }
                        );
                }
                return saveCreate;
            }
        }
        //xóa tài khoản
        private ICommand delete;
        public ICommand Delete
        {
            get
            {
                delete = new RelayCommand<DataGrid>((m) =>
                {
                    MessageBoxResult msg = MessageBox.Show("Chắc chưa?", "Xác nhận xóa", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (msg == MessageBoxResult.Yes)
                    {
                        List<NguoiDung> list = (m.SelectedItems as IList).ToList<NguoiDung>();
                        ListData.RemoveAll<NguoiDung>(list);
                        db.NguoiDungs.RemoveAll<NguoiDung>(list);
                        db.SaveChanges();
                    }
                    else
                    {
                        return;
                    }
                });
                return delete;
            }
        }
    }
}
