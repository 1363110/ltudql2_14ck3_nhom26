﻿using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModel.Utilities;

namespace ViewModel
{
    public class TimSanPhamViewModel : DataViewModel
    {
        public TimSanPhamViewModel()
        {
            base.Header = "Tìm kiếm sản phẩm";
            TenSP = "";
        }
        //binding danh sách danh mục
        private List<DanhMuc> listDanhMuc;
        public List<DanhMuc> ListDanhMuc
        {
            get
            {
                if (listDanhMuc == null)
                {
                    listDanhMuc = new List<DanhMuc>(db.DanhMucs.ToList());
                }
                return listDanhMuc;
            }
            set
            {
                if (listDanhMuc != value)
                {
                    listDanhMuc = value; OnPropertyChanged("ListDanhMuc");
                }
            }
        }
        //binding danh sách nhà cung cấp
        private List<NhaCungCap> listNhaCungCapTK;
        public List<NhaCungCap> ListNhaCungCapTK
        {
            get
            {
                if (listNhaCungCapTK == null)
                {
                    listNhaCungCapTK = new List<NhaCungCap>(db.NhaCungCaps.ToList());
                }
                return listNhaCungCapTK;
            }
            set
            {
                if (listNhaCungCapTK != value)
                {
                    listNhaCungCapTK = value; OnPropertyChanged("ListNhaCungCapTK");
                }
            }
        }
        //binding ô nhập tên sản phẩm tìm kiếm
        private string tenSP;
        public string TenSP
        {
            get
            {

                return tenSP;
            }
            set
            {
                if (tenSP != value)
                {
                    tenSP = value;
                    OnPropertyChanged("TenSP");
                }

            }
        }
        //binding ô giá thấp nhất
        private int? giaMin;
        public int? GiaMin
        {
            get
            {
                if (giaMin == null)
                {
                    giaMin = 0;
                }
                return giaMin;
            }
            set
            {
                if (giaMin != value)
                {
                    giaMin = value;
                    OnPropertyChanged("GiaMin");
                }

            }
        }
        //binding ô giá cao nhất
        private int? giaMax;
        public int? GiaMax
        {
            get
            {
                if (giaMax == null)
                {
                    giaMax = 99999999;
                }
                return giaMax;
            }
            set
            {
                if (giaMax != value)
                {
                    giaMax = value;
                    OnPropertyChanged("GiaMax");
                }

            }
        }
        //binding chọn combobox danh mục
        private DanhMuc dm;
        public DanhMuc DM
        {
            get { return dm; }
            set
            {
                if (dm != value)
                {
                    dm = value;
                    OnPropertyChanged("DM");
                }

            }
        }
        //binding chọn combobox nhà cung cấp
        private NhaCungCap ncc;
        public NhaCungCap NCC
        {
            get { return ncc; }
            set
            {
                if (ncc != value)
                {
                    ncc = value;
                    OnPropertyChanged("NCC");
                }

            }
        }
        //danh sách tìm kiếm
        ObservableCollection<HangHoa> listTimKiem()
        {
            IQueryable<HangHoa> query = db.HangHoas;
            if (TenSP.Length > 0)
            {
                query = query.Where(m => m.TenHangHoa.Contains(tenSP));
            }
            if (DM != null)
            {
                query = query.Where(m => m.MaDanhMuc == DM.MaDanhMuc);
            }
            if (NCC != null)
            {
                query = query.Where(m => m.MaNhaCungCap == NCC.MaNhaCungCap);
            }
            if (GiaMin != null)
            {
                query = query.Where(m => m.GiaBan >= GiaMin);
            }
            if (GiaMax != null)
            {
                query = query.Where(m => m.GiaBan <= GiaMax);
            }
            TotalPage = (int)Math.Ceiling(query.Count() * 1.0 / pageSize);
            var list = new ObservableCollection<HangHoa>(query.OrderBy(m=>m.MaHangHoa).Skip((CurPage-1)*pageSize).Take(pageSize).ToList());
            return list;
        }
        //binding danh sách tìm kiếm
        private ObservableCollection<HangHoa> listData;
        public ObservableCollection<HangHoa> ListData
        {
            get
            {
                if (listData == null)
                {
                    listData = new ObservableCollection<HangHoa>();
                }
                return listData;
            }
            set
            {
                if (listData != value)
                {
                    listData = value; OnPropertyChanged("ListData");
                }
            }
        }
        //xử lý tìm kiếm sản phẩm
        private ICommand searchSP;
        public ICommand SearchSP
        {
            get
            {
                if (searchSP == null)
                {
                    searchSP = new RelayCommand(() =>
                    {
                        CurPage = 1;
                        ListData = listTimKiem();
                        IndexView = true;
                    });
                }
                return searchSP;
            }
        }
        //làm mới ô nhập tìm kiếm
        private ICommand clearTK;
        public ICommand ClearTK
        {
            get
            {
                if (clearTK == null)
                {
                    clearTK = new RelayCommand(() =>
                    {
                        TenSP = "";
                        GiaMin = null;
                        GiaMax = null;
                        DM = null;
                        NCC = null;
                        //ListData = null;
                    });
                }
                return clearTK;
            }
        }
        //Xử lý phân trang
        private ICommand icNext;
        public ICommand IcNext
        {
            get
            {
                if (icNext == null)
                {
                    icNext = new RelayCommand(() =>
                    {
                        if (CurPage < TotalPage)
                        {
                            CurPage++;
                            ListData = listTimKiem();
                        }
                    });
                }
                return icNext;
            }
        }
        //
        private ICommand icPreview;
        public ICommand IcPreview
        {
            get
            {
                if (icPreview == null)
                {
                    icPreview = new RelayCommand(() =>
                    {
                        if (CurPage > 1)
                        {
                            CurPage--;
                            ListData = listTimKiem();
                        }
                    });
                }
                return icPreview;
            }
        }
    }
}
