﻿using Model;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModel.Utilities;
namespace ViewModel
{
    public class DanhMucViewModel:DataViewModel
    {
        public DanhMucViewModel()
        {
            base.Header = "Danh mục";
        }
        private ObservableCollection<DanhMuc> listData;
        public ObservableCollection<DanhMuc> ListData
        {
            get
            {
                if (listData == null)
                {
                    listData = new ObservableCollection<DanhMuc>(db.DanhMucs.ToList());
                }
                return listData;
            }
            set
            {
                if (listData != value)
                {
                    listData = value; OnPropertyChanged("ListData");
                }
            }
        }

        //lưu danh mục khi thêm mới
        private ICommand saveCreate;
        public ICommand SaveCreate
        {
            get
            {
                if (saveCreate == null)
                {
                    saveCreate = new RelayCommand<DataGrid>(
                        (m) =>
                        {
                            ObservableCollection<DataGridInput> datagrid = (ObservableCollection<DataGridInput>)m.ItemsSource;
                            var list = new List<DanhMuc>();
                            (datagrid.Where(c => c.Column1 != "").ToList()).ForEach<DataGridInput>(k =>
                            {
                                list.Add(new DanhMuc
                                {
                                    TenDanhMuc = k.Column1,
                                    MoTa = k.Column2
                                });
                            });
                            ListData.AddAll<DanhMuc>(list);
                            db.DanhMucs.InsertAll<DanhMuc>(list);
                            db.SaveChanges();
                            MessageBox.Show("Đã thêm:" + " " + list.Count().ToString() + " " + "Danh mục");
                            ClearGrid();
                        }
                        );
                }
                return saveCreate;
            }
        }

        //xóa danh mục
        private ICommand delete;
        public ICommand Delete
        {
            get
            {
                delete = new RelayCommand<DataGrid>((m) =>
                {
                    MessageBoxResult msg = MessageBox.Show("Chắc chưa?", "Xác nhận xóa", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (msg == MessageBoxResult.Yes)
                    {
                        List<DanhMuc> list = (m.SelectedItems as IList).ToList<DanhMuc>();
                        ListData.RemoveAll<DanhMuc>(list);
                        db.DanhMucs.RemoveAll<DanhMuc>(list);
                        db.SaveChanges();
                    }
                    else
                    {
                        return;
                    }
                });
                return delete;
            }
        }
    }
}
