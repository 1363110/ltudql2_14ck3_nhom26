﻿using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ViewModel.Utilities
{
    public class DataViewModel : ViewModelBase
    {
        public QLCHEntities db = new QLCHEntities();

        #region--binding combobox--
        //Danh sách nhà cung cấp trong Combobox
        private Dictionary<string, string> listNhaCungCap;
        public Dictionary<string, string> ListNhaCungCap
        {
            get
            {
                var list = new Dictionary<string, string>();

                db.NhaCungCaps.ForEach<NhaCungCap>(m =>
                {
                    list.Add(m.MaNhaCungCap.ToString(), m.TenNhaCungCap);
                });
                list.Add("", "chưa rõ");
                listNhaCungCap = list;
                return listNhaCungCap;
            }
        }
        //Danh sách người dùng trong Combobox
        private Dictionary<string, string> listNguoiDung;
        public Dictionary<string, string> ListNguoiDung
        {
            get
            {
                var list = new Dictionary<string, string>();

                db.NguoiDungs.ForEach<NguoiDung>(m =>
                {
                    list.Add(m.MaNguoiDung.ToString(), m.TenNguoiDung);
                });
                listNguoiDung = list;
                return listNguoiDung;
            }
        }
        //Danh sách sản phẩm trong Combobox
        private Dictionary<string, string> listSanPham;
        public Dictionary<string, string> ListSanPham
        {
            get
            {
                var list = new Dictionary<string, string>();

                db.HangHoas.ForEach<HangHoa>(m =>
                {
                    list.Add(m.MaHangHoa.ToString(), m.TenHangHoa);
                });
                listSanPham = list;
                return listSanPham;
            }
        }
        #endregion

        #region--binding phân trang--
        //Số record trong 1 trang
        public static int pageSize = 7;
        //trang hiện tại
        int curPage;
        public int CurPage
        {
            get { return curPage; }
            set
            {
                if (curPage != value)
                {
                    curPage = value;
                    OnPropertyChanged("CurPage");
                }

            }
        }
        //tổng số trang
        int totalPage;
        public int TotalPage
        {
            get { return totalPage; }
            set
            {
                if (totalPage != value)
                {
                    totalPage = value;
                    OnPropertyChanged("TotalPage");
                }

            }
        }
        #endregion

        //Thuộc tính Binding tạo Table DataGrid
        private ObservableCollection<DataGridInput> dataGrid;
        public ObservableCollection<DataGridInput> DataGrid
        {
            get
            {
                if (dataGrid == null)
                {
                    dataGrid = DataGridInput.GetTable(10);

                }
                return dataGrid;
            }
            set
            {
                if (dataGrid != value)
                {
                    dataGrid = value; OnPropertyChanged("DataGrid");
                }

            }
        }
        //Xóa dữ liệu ô nhập
        public void ClearGrid()
        {
            DataGrid.Clear();
            DataGrid = DataGridInput.GetTable(10);
        }

        //thông tin đăng nhập
        string taiKhoanSuDung;
        public string TaiKhoanSuDung
        {
            get { return taiKhoanSuDung; }
            set
            {
                if (taiKhoanSuDung != value)
                {
                    taiKhoanSuDung = value;
                    OnPropertyChanged("TaiKhoanSuDung");
                }

            }
        }
        private string nguoiSuDung;
        public string NguoiSuDung
        {
            get { return nguoiSuDung; }
            set
            {
                if (nguoiSuDung != value)
                {
                    nguoiSuDung = value;
                    OnPropertyChanged("NguoiSuDung");
                }

            }
        }

        #region ---Ẩn - hiện các view, template---
        // template Xem 
        private bool indexView = true;
        public bool IndexView
        {
            get { return indexView; }
            set
            {
                if (indexView != value)
                {
                    indexView = value; OnPropertyChanged("IndexView");
                }

            }
        }

        // template Thêm
        private bool addView = false;
        public bool AddView
        {
            get { return addView; }
            set
            {
                if (addView != value)
                {
                    addView = value; OnPropertyChanged("AddView");
                }

            }
        }
        // template Thêm chi tiết
        private bool addViewCT = false;
        public bool AddViewCT
        {
            get { return addViewCT; }
            set
            {
                if (addViewCT != value)
                {
                    addViewCT = value; OnPropertyChanged("AddViewCT");
                }

            }
        }

        // template Sửa
        private bool editView = false;
        public bool EditView
        {
            get { return editView; }
            set
            {
                if (editView != value)
                {
                    editView = value; OnPropertyChanged("EditView");
                }

            }
        }
        //template sửa chi tiết
        private bool editViewCT = false;
        public bool EditViewCT
        {
            get { return editViewCT; }
            set
            {
                if (editViewCT != value)
                {
                    editViewCT = value; OnPropertyChanged("EditViewCT");
                }

            }
        }

        //template xem chi tiết
        private bool detailView = false;
        public bool DetailView
        {
            get { return detailView; }
            set
            {
                if (detailView != value)
                {
                    detailView = value; OnPropertyChanged("DetailView");
                }
            }
        }

        #endregion

        #region-- Các command hiện, ẩn các view thêm sửa--
        // xử lý nút thêm mới 
        private ICommand create;
        public ICommand Create
        {
            get
            {
                if (create == null)
                {
                    create = new RelayCommand(() => { AddView = true; IndexView = false; EditView = false; });
                }
                return create;
            }
        }
        //
        private ICommand createCT;
        public ICommand CreateCT
        {
            get
            {
                if (createCT == null)
                {
                    createCT = new RelayCommand(() => { AddViewCT = true; DetailView = false; EditViewCT = false; });
                }
                return createCT;
            }
        }
        //nút sửa hạng mục
        private ICommand edit;
        public ICommand Edit
        {
            get
            {
                if (edit == null)
                {
                    edit = new RelayCommand(() => { AddView = false; IndexView = false; EditView = true; });
                }
                return edit;
            }
        }
        //
        private ICommand editCT;
        public ICommand EditCT
        {
            get
            {
                if (editCT == null)
                {
                    editCT = new RelayCommand(() => { AddViewCT = false; DetailView = false; EditViewCT = true; });
                }
                return editCT;
            }
        }
        //xử lý nút quay lại
        private ICommand goBack;
        public ICommand GoBack
        {
            get
            {
                if (goBack == null)
                {
                    goBack = new RelayCommand(() =>
                    {
                        IndexView = true; AddView = false; EditView = false; DetailView = false;
                    });
                }
                return goBack;
            }
        }
        //nút trở lại detail từ thêm, sửa chi tiết
        private ICommand goBackCT;
        public ICommand GoBackCT
        {
            get
            {
                if (goBackCT == null)
                {
                    goBackCT = new RelayCommand(() =>
                    {
                        AddViewCT = false; EditViewCT = false; DetailView = true;
                    });
                }
                return goBackCT;
            }
        }
        //trở lại view index từ view chi tiết
        private ICommand back;
        public ICommand Back
        {
            get
            {
                if (back == null)
                {
                    back = new RelayCommand(() =>
                    {
                        IndexView = true; DetailView = false;
                    });
                }
                return back;
            }
        }
        #endregion

        //xử lý clear dữ liệu ô nhập DataGrid - Dùng chung
        private ICommand clearCreate;
        public ICommand ClearCreate
        {
            get
            {
                clearCreate = new RelayCommand(() => { ClearGrid(); });
                return clearCreate;
            }
        }

        //lưu tất cả khi sửa - Dùng chung
        private ICommand saveAll;
        public ICommand SaveAll
        {
            get
            {
                if (saveAll == null)
                {
                    saveAll = new RelayCommand(() =>
                    {
                        db.SaveChanges();
                        MessageBox.Show("Lưu thành công!", "Thông báo", MessageBoxButton.OK, MessageBoxImage.Information);
                    });
                }
                return saveAll;
            }
        }
    }
}
