﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ViewModel.Utilities
{
    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable, INotifyDataErrorInfo
    {
        //Đồng bộ dữ liệu giữa View và ViewModel
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        // Thuộc tính hỗ trợ thẻ Tab
        public virtual string Header { get; set; }
        private ICommand icCloseTab;
        public ICommand IcCloseTab
        {
            get
            {
                if (icCloseTab == null)
                {
                    icCloseTab = new RelayCommand(() =>
                    {
                        EventHandler handler = this.RequestClose;
                        if (handler != null)
                            handler(this, EventArgs.Empty);
                    });
                }
                return icCloseTab;
            }
        }
        public event EventHandler RequestClose;

        // Xử lý thu gom rác
        public void Dispose()
        {
            //throw new NotImplementedException();
            this.OnDispose();
        }
        //
        protected virtual void OnDispose()
        {
        }
        //validate error
        private ConcurrentDictionary<string, List<string>> _errors = new ConcurrentDictionary<string, List<string>>();
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public void OnErrorsChanged(string propertyName)
        {
            var handler = ErrorsChanged;
            if (handler != null)
                handler(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public IEnumerable GetErrors(string propertyName)
        {
            List<string> errorsForName;
            _errors.TryGetValue(propertyName, out errorsForName);
            return errorsForName;
            //throw new NotImplementedException();
        }

        public bool HasErrors
        {
            get
            {
                //throw new NotImplementedException();
                return _errors.Any(kv => kv.Value != null && kv.Value.Count > 0);
            }
        }

    }
}
