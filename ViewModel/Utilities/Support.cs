﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Windows;
using System.Windows.Input;
using ViewModel.Utilities;

namespace ViewModel.Utilities
{
    //Hỗ trợ hiển thị nhập liệu trên DataGrid
    public class DataGridInput
    {
        public string STT { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }
        public string Column5 { get; set; }
        public string Column6 { get; set; }
        public string Column7 { get; set; }
        public string Column8 { get; set; }
        public string Column9 { get; set; }
        public string Column10 { get; set; }
        public string Column11 { get; set; }
        public static ObservableCollection<DataGridInput> GetTable(int x)
        {
            var table = new ObservableCollection<DataGridInput>();
            for (int i = 1; i <= x; i++)
            {
                table.Add(new DataGridInput
                {
                    STT = i.ToString(),
                    Column1 = "",
                    Column2 = "",
                    Column3 = "",
                    Column4 = "",
                    Column5 = "",
                    Column6 = "",
                    Column7 = "",
                    Column8 = "",
                    Column9 = "",
                    Column10 = "",
                    Column11 = "",

                });
            }
            return table;
        }

    }

    public static class MyExtensionMethod
    {
        // Hổ trợ ObservableCollection
        public static void RemoveAll<T>(this ObservableCollection<T> x, List<T> y)
        {
            y.ForEach<T>(m => { x.Remove(m); });
        }
        public static void AddAll<T>(this ObservableCollection<T> x, List<T> y)
        {
            y.ForEach(m => x.Add(m));
        }
        //Hổ trợ DataContext thêm dữ liệu vào List
        public static void InsertAll<T>(this DbSet<T> x, List<T> y) where T : class
        {
            y.ForEach<T>(m => x.Add(m));
        }
        //xóa dữ liệu được chọn khỏi List
        public static void RemoveAll<T>(this DbSet<T> x, List<T> y) where T : class
        {
            y.ForEach<T>(m => x.Remove(m));
        }
        //xử lý IList sang List: thêm item vào
        public static List<T> ToList<T>(this IList obj) where T : class
        {
            var list = new List<T>();
            foreach (T item in obj)
            {
                list.Add(item);
            }
            return list;
        }

        // ép kiểu string qua int
        public static int? ToInt(this string obj)
        {
            try
            {
                if (obj == "")
                {
                    return null;
                }
                return Int32.Parse(obj);
            }
            catch
            {
                return 0;
            }
        }
        // ép kiểu string qua decimal
        public static decimal? ToDecimal(this string obj)
        {
            try
            {

                if (obj == "")
                {
                    return null;
                }
                return Decimal.Parse(obj);
            }
            catch
            {
                return 0;
            }
        }
        //ép kiểu string qua DateTime
        public static DateTime? ToDateTime(this string obj)
        {
            if (obj == "")
            {
                return null;
            }
            return DateTime.ParseExact(obj, "dd/MM/yyyy", null);
        }
        // Foreach IEnumable
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }
    }
}
