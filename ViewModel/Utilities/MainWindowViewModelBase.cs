﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ViewModel.Utilities
{
    public class MainWindowViewModelBase
    {
        private ObservableCollection<ViewModelBase> listViewModel;
        public ObservableCollection<ViewModelBase> ListViewModel
        {
            get
            {
                if (listViewModel == null)
                {
                    listViewModel = new ObservableCollection<ViewModelBase>();
                    listViewModel.CollectionChanged += listViewModel_CollectionChanged;
                }
                return listViewModel;
            }
        }

        //mở thêm tab, đóng tab
        void listViewModel_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
            {
                foreach (ViewModelBase item in e.NewItems)
                {
                    item.RequestClose += RemoveTab;
                }
            }
            if (e.OldItems != null && e.OldItems.Count != 0)
            {
                foreach (ViewModelBase item in e.OldItems)
                {
                    item.RequestClose -= RemoveTab;
                }
            }
        }

        //
        void RemoveTab(object sender, EventArgs e)
        {
            var table = sender as ViewModelBase;
            // Gọi bộ dọn rác
            table.Dispose();

            //Xóa khỏi Tab
            ListViewModel.Remove(table);
        }
        void SetActiveTab(ViewModelBase x)
        {
            ICollectionView y = CollectionViewSource.GetDefaultView(ListViewModel);
            if (y != null)
            {
                y.MoveCurrentTo(x);
            }
        }
        public void AddViewModel<T>() where T : class
        {
            var a = ListViewModel.FirstOrDefault(m => m is T);
            if (a == null)
            {
                T item = (T)Activator.CreateInstance(typeof(T));
                a = item as ViewModelBase;
                ListViewModel.Add(a);
            }
            SetActiveTab(a);
        }
    }
}
