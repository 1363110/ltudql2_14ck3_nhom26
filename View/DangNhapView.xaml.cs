﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for DangNhapView.xaml
    /// </summary>
    public partial class DangNhapView : Window
    {
        public DangNhapView()
        {
            InitializeComponent();
        }
        public void Button_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new QLCHEntities())
            {
               
                IQueryable<NguoiDung> query = db.NguoiDungs.Where(m => m.TaiKhoan == txtTaiKhoan.Text && m.MatKhau == txtMatKhau.Password);
                int tk = query.Count();
             
                if (tk < 1)
                {
                    txtblLoi.Foreground = Brushes.Red;
                    txtblLoi.FontStyle = FontStyles.Italic;
                    txtblLoi.FontFamily = new FontFamily("Times New Roman");
                    txtblLoi.FontSize = 14;
                    txtblLoi.Text = "Tài khoản, mật khẩu không đúng";
                    return;
                }
                else
                {
                    int q;
                    int? quyen = db.NguoiDungs.SingleOrDefault(m => m.TaiKhoan == txtTaiKhoan.Text && m.MatKhau == txtMatKhau.Password).MaLoai;
                    string ten = db.NguoiDungs.Single(m => m.TaiKhoan == txtTaiKhoan.Text && m.MatKhau == txtMatKhau.Password).TenNguoiDung;
                    if (quyen == null)
                    {
                        q = 0;
                    }
                    else
                    {
                        q = (int)quyen;
                    }
                    var frmMain = new MainWindow(q, ten);
                        frmMain.Show();
                        this.Close();
                }

            }
          
        }

        private void btnThoat_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
