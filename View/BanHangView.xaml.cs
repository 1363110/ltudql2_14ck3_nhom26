﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for BanHangView.xaml
    /// </summary>
    public partial class BanHangView : UserControl
    {
        public BanHangView()
        {
            InitializeComponent();
            
        }
        private void TextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            DateTime today = DateTime.Now;
            var txt = sender as TextBlock;
            txt.Text = String.Format("{0:dd/MM/yyyy}", today);

        }

    }
}
