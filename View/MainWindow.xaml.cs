﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public MainWindow(int i, string ten)
        {
            InitializeComponent();
            txtblTaiKhoan.Text = ten;
            if (i == 1)
            {
                EnabledMenuUser(true, "");
            }
            else if (i == 2)
            {
                EnabledMenu(true, "");
            }
            else
            {
                EnabledMenuUser(false, "");
            }
        }

        public void EnabledMenu(bool e, string id)
        {
            rbgrDanhMuc.IsEnabled = e;
            rbgrNhaCungCap.IsEnabled = e;
            rbgrSanPham.IsEnabled = e;
            rbgrTimSp.IsEnabled = e;
            rbgrBanHang.IsEnabled = e;
            rbgrNhapHang.IsEnabled = e;
            rbgrNhanVien.IsEnabled = e;
        }
        public void EnabledMenuUser(bool e, string id)
        {
            rbgrDanhMuc.IsEnabled = e;
            rbgrNhaCungCap.IsEnabled = e;
            rbgrSanPham.IsEnabled = e;
            rbgrTimSp.IsEnabled = e;
            rbgrBanHang.IsEnabled = !e;
            rbgrNhapHang.IsEnabled = !e;
            rbgrNhanVien.IsEnabled = !e;
        }
       
        private void rbSplitThoat_Click(object sender, RoutedEventArgs e)
        {
            DangNhapView dn = new DangNhapView();
            dn.Show();
            this.Close();
        }

        private void rbappExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
